Loadable Kernel Module Example
==============================

Taken directly from [this blog
post](https://blog.sourcerer.io/writing-a-simple-linux-kernel-module-d9dc3762c234?gi=1305eb75718d).
A couple of example files demonstrating how to create simple loadable kernel
modules in Linux.

Building
--------

As stated in the blog post, you need build essential and your linux headers.

``` sh
$ apt install build-essential linux-headers-`uname -r`
```

Then you can simply use the Makefile to build the modules, and load them as
usual.

``` sh
$ make
$ sudo insmod lkm_example.ko
$ sudo dmesg
$ sudo rmmod lkm_example
```

The second example provides a character device based interface, and to talk
with this in userland  you need to load the module, grab the device major
number from printk, and create a new device file using that.  This file can
then be treated as a character device, like `/dev/urandom`.

``` sh
$ sudo insmod lkm_example2.ko
$ sudo dmesg
$ sudo mknod /dev/lkm_example2 c <MAJOR_NUMBER> 0
$ head /dev/lkm_example2
$ sudo rm /dev/lkm_example2
$ sudo rmmod lkm_example2
```
